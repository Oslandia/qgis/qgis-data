#!/bin/bash

dropdb qgis_data
createdb qgis_data

psql qgis_data -c "create extension postgis;"
psql qgis_data -c "create role qgisuser;" 
psql qgis_data -f caen.sql
