# Qgis Data

This project contrains set of data to test and demonstrate QGIS features.


## Projects

- external_storage.qgz : Lille project and dataset that you can use to test a WebDAV external storage. You need to have a Nextcloud instance installed and you need to change the *Storage URL* within the attachmet widget.

### mapserv : Initialize database

```shell
$ createdb mapserv_db
$ psql mapserv_db
SQL> create extension postgis;

$ ~/work/depends/gdal-3.1.1/install/bin/ogr2ogr -f "PostgreSQL" "PG:host=127.0.0.1 user=postgres dbname=mapserv_db" -overwrite -nlt PROMOTE_TO_MULTI ~/work/qgis_sample_data/shapefiles/regions.shp
```
